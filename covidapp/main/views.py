from datetime import datetime
from flask import render_template, session, redirect, url_for
from . import main
from .. import db

@main.route('/', methods=['GET', 'POST']) 
def accueil():
    return render_template('main/accueil.html', current_time=datetime.utcnow())

@main.route('/flash_infos', methods=['GET', 'POST'])
def flashInfo():
    return render_template('main/flashinfos.html')

